from flask import Flask, render_template, redirect, url_for, request
import db
app = Flask(__name__)

from pymongo import MongoClient
from bson.objectid import ObjectId

@app.route('/')
def home():
    todoList = db.db.todo.find()
    data = todoList
    dataListe = []
    for liste in data:
        key = liste['_id']
        msg = liste['msg']
        result = {'key': key, 'msg': msg}
        dataListe.append(result)
    return render_template('home.html',data=dataListe)

@app.route('/addtodo/',methods = ['POST'])
def addTodo():
    result = request.form
    msg = result['msg']
    data = {"msg": msg}
    db.db.todo.insert_one(data) 
    return redirect(url_for('home'))

@app.route('/deltodo/',methods = ['POST'])
def delTodo():
    result = request.form
    key = {'_id': ObjectId(result['key'])}
    db.db.todo.delete_one(key)
    return redirect(url_for('home'))

@app.route('/updatetodo/',methods = ['POST'])
def updatetodo():
    result = request.form
    print(result)
    msg = result['msg']
    key = result['key']

    keyDoc = {'_id': ObjectId(key)}
    newMsg ={'$set':{ 'msg' : msg }}
    
    db.db.todo.update_one(keyDoc,newMsg)
    return redirect(url_for('home'))


if __name__ == '__main__':
    app.run(host='0.0.0.0',port=8080)
